import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:tomisha_task/generated/assets.gen.dart';

class ArbeitnehmerPage extends StatelessWidget {
  const ArbeitnehmerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(
      builder: (context, orientation, screenType) {
        return SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: Adaptive.w(2.w)),
          child: Column(
            children: [
              const SizedBox(
                height: 32,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 50, vertical: 24),
                child: Text(
                  'Drei einfache Schritte zu deinem neuen Job',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 24),
                ),
              ),
              Stack(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SvgPicture.asset(
                        Assets.images.undrawProfileDataReV81r.path,
                        height: Adaptive.h(4.h),
                      ),
                      Row(
                        children: [
                          Text(
                            '1.',
                            style: TextStyle(
                              color: const Color(0xFF718096),
                              fontSize: Adaptive.h(2.h),
                            ),
                          ),
                          const Flexible(
                            child: Text(
                              'Erstellen dein Lebenslauf',
                              style: TextStyle(color: Color(0xFF718096)),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    children: [
                      const SizedBox(
                        width: 48,
                      ),
                      Text(
                        '2.',
                        style: TextStyle(
                          color: const Color(0xFF718096),
                          fontSize: Adaptive.h(2.h),
                        ),
                      ),
                      const Flexible(
                        child: Text(
                          'Erstellen dein Lebenslauf',
                          style: TextStyle(color: Color(0xFF718096)),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  SvgPicture.asset(
                    Assets.images.undrawTask31wc.path,
                    height: Adaptive.h(4.h),
                  ),
                ],
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    children: [
                      const SizedBox(
                        width: 56,
                      ),
                      Text(
                        '3.',
                        style: TextStyle(
                          color: const Color(0xFF718096),
                          fontSize: Adaptive.h(2.h),
                        ),
                      ),
                      const Flexible(
                        child: Text(
                          'Mit nur einem Klick bewerben',
                          style: TextStyle(color: Color(0xFF718096)),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  SvgPicture.asset(
                    Assets.images.undrawPersonalFile222m.path,
                    height: Adaptive.h(4.h),
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
