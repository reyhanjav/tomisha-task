// ignore_for_file: depend_on_referenced_packages, cascade_invocations

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:tomisha_task/generated/assets.gen.dart';
import 'package:tomisha_task/widgets/widgets.dart';

class LandingPage extends StatelessWidget {
  const LandingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(
      builder: (context, orientation, screenType) {
        return Scaffold(
          appBar: AppBar(
            shape: const ContinuousRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30),
              ),
            ),
            backgroundColor: Colors.white,
            actions: [TextButton(onPressed: () {}, child: const Text('Login'))],
          ),
          body: Stack(
            children: [
              Visibility(
                visible: screenType == ScreenType.mobile,
                replacement: ClipPath(
                  clipper: BottomWaveClipper(),
                  child: Container(
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)],
                      ),
                    ),
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 24,
                        ),
                        Container(
                          constraints: BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width / 3,
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              const Text(
                                'Deine Job website',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 56),
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              GradientButton(
                                title: 'Kostenlos Registrieren',
                                onPressed: () {},
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 24,
                        ),
                        CircleAvatar(
                          radius: 140,
                          backgroundColor: Colors.white,
                          backgroundImage: AssetImage(
                            Assets.images.undrawAgreementAajrPng.path,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                child: Container(
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)],
                    ),
                  ),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 18,
                      ),
                      const Text(
                        'Deine Job website',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 56),
                      ),
                      const SizedBox(
                        height: 18,
                      ),
                      SvgPicture.asset(
                        Assets.images.undrawAgreementAajr.path,
                        width: MediaQuery.of(context).size.width,
                      ),
                    ],
                  ),
                ),
              ),
              Visibility(
                visible: screenType == ScreenType.mobile,
                child: Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    height: 60,
                    width: double.infinity,
                    decoration: const BoxDecoration(
                      color: Colors.white, //new Color.fromRGBO(255, 0, 0, 0.0),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GradientButton(
                            title: 'Kostenlos Registrieren',
                            onPressed: () {},
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class BottomWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path
      ..lineTo(0, size.height - 40)
      ..quadraticBezierTo(
        size.width / 4,
        size.height - 80,
        size.width / 2,
        size.height - 40,
      )
      ..quadraticBezierTo(
        size.width - (size.width / 4),
        size.height,
        size.width,
        size.height - 40,
      )
      ..lineTo(size.width, 0)
      ..close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
