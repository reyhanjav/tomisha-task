import 'package:flutter/material.dart';
import 'package:tomisha_task/arbeitgeber/view/arbeitgeber_page.dart';
import 'package:tomisha_task/arbeitnehmer/arbeitnehmer.dart';
import 'package:tomisha_task/temporarburo/view/temporarburo_page.dart';

class TabPage extends StatefulWidget {
  const TabPage({super.key});

  @override
  State<TabPage> createState() => _TabPageState();
}

class _TabPageState extends State<TabPage> with TickerProviderStateMixin {
  late final TabController _controller;
  @override
  void initState() {
    super.initState();
    _controller = TabController(
      length: 3,
      vsync: this,
      animationDuration: Duration.zero, // change animation to 0ms
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TabBar(
            labelColor: Colors.black,
            isScrollable: true,
            controller: _controller,
            tabs: const [
              Tab(
                key: Key('arbeitnehmer_tab'),
                text: 'Arbeitnehmber',
              ),
              Tab(
                key: Key('arbeitgeber_tab'),
                text: 'Arbeitgeber',
              ),
              Tab(
                key: Key('temporarburo_tab'),
                text: 'Temporärbüro',
              ),
            ],
          ),
          Expanded(
            child: TabBarView(
              physics: const NeverScrollableScrollPhysics(),
              controller: _controller,
              children: const [
                ArbeitnehmerPage(),
                ArbeitgeberPage(),
                TemporarBuroPage()
              ],
            ),
          ),
        ],
      ),
    );
  }
}
