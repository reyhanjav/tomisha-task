import 'package:flutter/material.dart';
import 'package:tomisha_task/home/home.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = PageController();
    return PageView(
      scrollDirection: Axis.vertical,
      pageSnapping: false,
      controller: controller,
      children: const [LandingPage(), TabPage()],
    );
  }
}
