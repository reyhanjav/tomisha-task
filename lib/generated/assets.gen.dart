/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/undraw_Profile_data_re_v81r.svg
  SvgGenImage get undrawProfileDataReV81r =>
      const SvgGenImage('assets/images/undraw_Profile_data_re_v81r.svg');

  /// File path: assets/images/undraw_about_me_wa29.svg
  SvgGenImage get undrawAboutMeWa29 =>
      const SvgGenImage('assets/images/undraw_about_me_wa29.svg');

  /// File path: assets/images/undraw_agreement_aajr-png.png
  AssetGenImage get undrawAgreementAajrPng =>
      const AssetGenImage('assets/images/undraw_agreement_aajr-png.png');

  /// File path: assets/images/undraw_agreement_aajr.svg
  SvgGenImage get undrawAgreementAajr =>
      const SvgGenImage('assets/images/undraw_agreement_aajr.svg');

  /// File path: assets/images/undraw_business_deal_cpi9.svg
  SvgGenImage get undrawBusinessDealCpi9 =>
      const SvgGenImage('assets/images/undraw_business_deal_cpi9.svg');

  /// File path: assets/images/undraw_job_offers_kw5d.svg
  SvgGenImage get undrawJobOffersKw5d =>
      const SvgGenImage('assets/images/undraw_job_offers_kw5d.svg');

  /// File path: assets/images/undraw_personal_file_222m.svg
  SvgGenImage get undrawPersonalFile222m =>
      const SvgGenImage('assets/images/undraw_personal_file_222m.svg');

  /// File path: assets/images/undraw_swipe_profiles1_i6mr.svg
  SvgGenImage get undrawSwipeProfiles1I6mr =>
      const SvgGenImage('assets/images/undraw_swipe_profiles1_i6mr.svg');

  /// File path: assets/images/undraw_task_31wc.svg
  SvgGenImage get undrawTask31wc =>
      const SvgGenImage('assets/images/undraw_task_31wc.svg');

  /// List of all assets
  List<dynamic> get values => [
        undrawProfileDataReV81r,
        undrawAboutMeWa29,
        undrawAgreementAajrPng,
        undrawAgreementAajr,
        undrawBusinessDealCpi9,
        undrawJobOffersKw5d,
        undrawPersonalFile222m,
        undrawSwipeProfiles1I6mr,
        undrawTask31wc
      ];
}

class Assets {
  Assets._();

  static const $AssetsImagesGen images = $AssetsImagesGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider() => AssetImage(_assetName);

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    Color? color,
    BlendMode colorBlendMode = BlendMode.srcIn,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    Clip clipBehavior = Clip.hardEdge,
    bool cacheColorFilter = false,
    SvgTheme? theme,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      color: color,
      colorBlendMode: colorBlendMode,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
      theme: theme,
    );
  }

  String get path => _assetName;
}
