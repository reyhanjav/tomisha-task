import 'package:tomisha_task/app/app.dart';
import 'package:tomisha_task/bootstrap.dart';

void main() {
  bootstrap(() => const App());
}
